# Privacy

## Effective date: August 24, 2022

This privacy policy is designed to provide transparency to your use of the Bonch.Timetable applications.


## User provided information

If you request information or technical support, your personal data (such as your email address) is used only to respond to specific inquiry.


## Information gathering and usage

No information gathering and/or usage performed.


## Your data

We do not collect any of your data.


## Advertising

No advertising performed.


## Policy changes

This privacy policy may be updated from time to time for any reason. We will notify you of any changes to Bonch.Timetable Privacy Policy by posting the new Privacy Policy.
