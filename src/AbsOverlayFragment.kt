package com.somobu.timetable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment

abstract class AbsOverlayFragment : Fragment() {

    abstract fun onCreateContent(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View

    abstract fun onRetry()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_overlay, container, false)
        root.findViewById<View>(R.id.retry).setOnClickListener { onRetry() }

        val contentView: LinearLayout = root.findViewById(R.id.success)
        contentView.addView(onCreateContent(inflater, contentView, savedInstanceState))

        return root
    }

    fun setPleaseWait() {
        val root = view ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.VISIBLE
        root.findViewById<View>(R.id.error_retry).visibility = View.GONE
        root.findViewById<View>(R.id.success).visibility = View.GONE
    }

    fun setContent() {
        val root = view ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.GONE
        root.findViewById<View>(R.id.error_retry).visibility = View.GONE
        root.findViewById<View>(R.id.success).visibility = View.VISIBLE
    }

    fun setError(e: Exception?) {
        e!!.printStackTrace()

        val root = view ?: return
        root.findViewById<View>(R.id.please_wait).visibility = View.GONE
        root.findViewById<View>(R.id.error_retry).visibility = View.VISIBLE
        root.findViewById<View>(R.id.success).visibility = View.GONE
        (root.findViewById<View>(R.id.error_message) as TextView).text = e.localizedMessage
    }

}