package com.somobu.timetable.calendar

import android.app.AlertDialog
import android.content.ContentUris
import android.content.Context
import android.os.Handler
import android.provider.CalendarContract
import com.somobu.timetable.R


class CalendarCleanerTask(private val context: Context) : Thread() {

    private val handler: Handler = Handler()

    private val blockingDialog: AlertDialog = AlertDialog.Builder(context)
        .setMessage(R.string.insertion_in_progress).setCancelable(false).show()

    override fun run() {
        var caughtException: Exception? = null
        try {
            val projection = arrayOf(
                CalendarContract.Events._ID,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.CUSTOM_APP_PACKAGE
            )
            val selection =
                "(" + CalendarContract.Events.CUSTOM_APP_PACKAGE + " == \"" + context.packageName + "\"" + " ) AND ( deleted != 1 )"

            val cursor = context.contentResolver.query(
                CalendarContract.Events.CONTENT_URI, projection, selection, null, null
            )

            if (cursor != null && cursor.count > 0 && cursor.moveToFirst()) {
                do {
                    val deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, cursor.getLong(0))
                    context.contentResolver.delete(deleteUri, null, null)
                } while (cursor.moveToNext())

                cursor.close()
            }
        } catch (ex: Exception) {
            caughtException = ex
        }

        val finalEx = caughtException
        handler.post { onPostExecute(finalEx) }
    }

    private fun onPostExecute(e: Exception?) {
        blockingDialog.dismiss()

        val message: String
        if (e == null) {
            message = context.getString(R.string.cleanup_success)
        } else {
            e.printStackTrace()
            message = context.getString(R.string.cleanup_failed, e.localizedMessage)
        }

        AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(R.string.insertion_ok, null)
            .show()
    }
}
