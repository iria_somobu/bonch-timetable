package com.somobu.timetable.calendar

data class CalendarEvent(
    val id: Long,
    val calendar: Long,
    val title: String,
    val since: Long,
    val pkg: String
)
