package com.somobu.timetable.calendar

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.CalendarContract
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import com.somobu.timetable.R
import com.somobu.timetable.MainActivity


class InsertionFragment : Fragment() {
    private var selectedCalendar: CalendarInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.calendar_inserter, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.clear_calendars) {
            showCleanCalendars()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_insertion, container, false)
        setupButton(root)
        root.findViewById<View>(R.id.rq_perm).setOnClickListener { v: View? ->
            requestPermissions(
                arrayOf("android.permission.READ_CALENDAR", "android.permission.WRITE_CALENDAR"),
                243
            )
        }
        setup(root)
        return root
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            hidePermOverlay(null)
            setupCalendarSpinner(null)
        }
    }

    override fun onResume() {
        super.onResume()
        setup(view)
    }

    private fun setup(root: View?) {
        if (root == null) return
        var permGranted = true
        try {
            if (Build.VERSION.SDK_INT > 23) {
                if (requireContext().checkSelfPermission("android.permission.WRITE_CALENDAR") != PackageManager.PERMISSION_GRANTED
                    && requireContext().checkSelfPermission("android.permission.READ_CALENDAR") != PackageManager.PERMISSION_GRANTED
                ) {
                    permGranted = false
                }
            }
        } catch (e: Exception) {
//            (activity as IErrorShowing?).userErrorMessage(e)
        }
        if (permGranted) {
            hidePermOverlay(root)
            setupCalendarSpinner(root)
        } else {
            root.findViewById<View>(R.id.no_permissions_warn).visibility = View.VISIBLE
        }
    }

    private fun setupButton(root: View) {
        root.findViewById<View>(R.id.main_btn_start).setOnClickListener { _: View? ->
            val bounds: CalendarFillerTask.FillMode

            when ((root.findViewById<View>(R.id.main_spinner_bounds) as Spinner).selectedItemPosition) {
                0 -> bounds = CalendarFillerTask.FillMode.TWO_WEEKS
                else -> bounds = CalendarFillerTask.FillMode.ALL_EVENTS
            }

            val group = PreferenceManager.getDefaultSharedPreferences(context).getString(MainActivity.GROUP_ID, null)
            val cft = CalendarFillerTask(requireContext(), selectedCalendar!!, bounds, group!!)
            cft.start()
        }
    }

    private fun hidePermOverlay(_root: View?) {
        var root = _root
        if (root == null) root = view
        if (root == null) return

        root.findViewById<View>(R.id.no_permissions_warn).visibility = View.GONE
    }

    private fun setupCalendarSpinner(_root: View?) {
        var root = _root
        if (root == null) root = view
        if (root == null) return

        val rememberedCalendarId =
            PreferenceManager.getDefaultSharedPreferences(context).getString("MAIN_UI_CAL_ID", null)

        val sp = root.findViewById<Spinner>(R.id.main_spinner_calendar)

        val calendars = ArrayList<CalendarInfo>()
        var savedSelectedCalendarId = -1

        try {
            val contentResolver = requireActivity().contentResolver

            val cursor = contentResolver.query(
                Uri.parse("content://com.android.calendar/calendars"),
                arrayOf("_id", CalendarContract.Calendars.CALENDAR_DISPLAY_NAME),
                null,
                null,
                null
            )

            var index = 0
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    val id = cursor.getString(0)
                    val displayName = cursor.getString(1)
                    calendars.add(CalendarInfo(id, displayName))
                    if (id == rememberedCalendarId) savedSelectedCalendarId = index
                    index++
                }
                cursor.close()
            }
        } catch (e: Exception) {
//            (activity as IErrorShowing?).userErrorMessage(e)
        }

        if (calendars.size > 0) selectedCalendar = calendars[0]
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, calendars)
        sp.adapter = adapter
        sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedCalendar = adapter.getItem(position)
                PreferenceManager.getDefaultSharedPreferences(activity).edit()
                    .putString("MAIN_UI_CAL_ID", "" + selectedCalendar!!.id).apply()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        if (savedSelectedCalendarId > -1) sp.setSelection(savedSelectedCalendarId)
    }

    private fun showCleanCalendars() {
        AlertDialog.Builder(context)
            .setMessage(R.string.cleanup_title)
            .setNegativeButton(R.string.cleanup_reject, null)
            .setPositiveButton(R.string.cleanup_confirm) { _: DialogInterface?, _: Int ->
                CalendarCleanerTask(requireContext()).start()
            }
            .show()
    }
}
