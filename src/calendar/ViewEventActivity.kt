package com.somobu.timetable.calendar

import android.app.Activity
import android.os.Bundle
import android.provider.CalendarContract
import android.view.View
import android.widget.TextView
import com.somobu.timetable.R


class ViewEventActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_event)

        (findViewById<View>(R.id.info) as TextView).text = intent.getStringExtra(CalendarContract.EXTRA_CUSTOM_APP_URI)

        findViewById<View>(R.id.close_activity).setOnClickListener { _: View? ->
            setResult(RESULT_OK)
            finish()
        }
    }
}
