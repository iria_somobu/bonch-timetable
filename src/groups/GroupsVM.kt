package com.somobu.timetable.groups

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.somobu.sutmodel.GroupsJson
import com.somobu.timetable.API

class GroupsVM(val myapp: android.app.Application) : AndroidViewModel(myapp) {

    class GroupsData {
        var isLoaded = false
        var e: Exception? = null
        var data: GroupsJson? = null
    }

    private val data: MutableLiveData<GroupsData> by lazy {
        MutableLiveData(GroupsData()).also { loadGroups() }
    }

    fun loadGroups() {
        object : Thread() {

            override fun run() {
                val d = GroupsData()
                d.isLoaded = true

                try {
                    d.data = API.getGroups(myapp.applicationContext)
                } catch (e: java.lang.Exception) {
                    d.e = e
                }

                data.postValue(d)
            }

        }.start()
    }

    fun groups(): LiveData<GroupsData> {
        return data
    }
}