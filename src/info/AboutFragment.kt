package com.somobu.timetable.info

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.somobu.timetable.R


class AboutFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_about, container, false)
        val text = root.findViewById<TextView>(R.id.centered_text)
        text.text = Html.fromHtml(getString(R.string.message_about))

        // Make the textview clickable
        text.movementMethod = LinkMovementMethod.getInstance()
        return root
    }

}