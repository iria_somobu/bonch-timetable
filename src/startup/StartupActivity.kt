package com.somobu.timetable.startup

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentContainerView
import com.somobu.timetable.R
import com.somobu.timetable.MainActivity
import com.somobu.timetable.groups.GroupPickerFragment

class StartupActivity : FragmentActivity(), GroupPickerFragment.OnGroupPickedListener {

    companion object {

        /**
         * Это - ключ SharedPref'ов, под которым мы храним код последнего перевыбора семестра.
         *
         * Перевыбор семестра нужен для того, чтобы уведомить юзера о собственно смене сема, чтобы
         * юзер ручками выбрал свой новый факультет, курс и группу.
         *
         * @see .ACTUAL_SEMESTER, в котором записан текущий код выбранного семестра
         */
        const val UPDATED_SEMESTER_KEY = "updated_semester"

        /**
         * Код актуального семестра в формате год_начала.год_окончания/семестр
         * Не имеет отношения к бончевой нумерации семестров
         */
        const val ACTUAL_SEMESTER = "22.23/2"

        const val FORCE_PICK = "force_pick"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startup)
    }

    override fun onResume() {
        super.onResume()

        val forcePick = intent.getBooleanExtra(FORCE_PICK, false)

        val container = findViewById<FragmentContainerView>(R.id.fragment_container_view)
        val fragment = container.getFragment<GroupPickerFragment>()
        fragment.pickListener = this

        if(!forcePick) {
            val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            val selectedGroup = prefs.getString(MainActivity.GROUP_ID, null)
            if (selectedGroup != null) {
                val sem = prefs.getString(UPDATED_SEMESTER_KEY, "none")
                if (sem != null && sem == ACTUAL_SEMESTER) {
                    openMainActivity()
                }
            }
        }
    }

    override fun onGroupPicked(faculty: String, group: String) {
        val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        prefs.edit()
            .putString(MainActivity.GROUP_ID, group)
            .putString(UPDATED_SEMESTER_KEY, ACTUAL_SEMESTER)
            .apply()

        openMainActivity()
    }

    private fun openMainActivity() {
        startActivity(Intent(this@StartupActivity, MainActivity::class.java))
        finish()
    }
}