package com.somobu.timetable.timetables

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.somobu.timetable.R
import com.somobu.sutmodel.GroupsJson
import com.somobu.timetable.AbsOverlayFragment

abstract class AbsTtFragment : AbsOverlayFragment() {

    abstract fun getSelectionRef(): TimetableVM.Ref

    abstract fun navToGroup(group: Int)

    abstract fun navToTeacher(teacher: String)

    open fun eventsLoaded(groups: GroupsJson, data: List<MyRecyclerAdapter.Entry>) {}

    private val timetableVM: TimetableVM by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()

        timetableVM.timetable(getSelectionRef())
            .observe(this) { table: TimetableVM.TimetableData ->
                if (table.isLoaded) {
                    if (table.data != null) {
                        onEventsLoaded(table.groups!!, table.data!!, table.firstPresentRef)
                    } else if (table.e != null) {
                        setError(table.e)
                    }
                } else {
                    setPleaseWait()
                }
            }
    }

    override fun onCreateContent(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.content_events, container, false);
    }

    private fun onEventsLoaded(groups: GroupsJson, data: List<MyRecyclerAdapter.Entry>, scrollTo: MyRecyclerAdapter.Entry?) {
        setContent()

        val root = view ?: return
        val list = root.findViewById<RecyclerView>(R.id.list)
        val adapter = MyRecyclerAdapter(
            data,
            { e -> navToGroup(e) },
            { e -> navToTeacher(e) },
            getSelectionRef().src == TimetableVM.Src.BY_GROUP
        )
        list.layoutManager = LinearLayoutManager(requireContext())
        list.adapter = adapter

        if (scrollTo != null) {
            list.scrollToPosition(data.indexOf(scrollTo) - 1)
        } else {
            list.scrollToPosition(data.size - 1)
        }

        eventsLoaded(groups, data)
    }

    override fun onRetry() {
        timetableVM.loadTable(getSelectionRef())
    }

}