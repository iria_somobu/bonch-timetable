package com.somobu.timetable.timetables

import android.annotation.SuppressLint
import com.somobu.sutmodel.Pair
import java.text.SimpleDateFormat
import java.util.*

data class Event(
    val pairNo: Int,
    val pairName: String,

    val since: Long,
    val to: Long,

    val title: String,
    val location: String,
    val type: Pair.Type?,

    val teachers: List<String>,
    val teacherIds: List<Int>,

    val groups: List<String>,
    val groupIds: List<Int>
) {

    @SuppressLint("SimpleDateFormat")
    fun atTheSameDay(curr: Event): Boolean {
        val fmt = SimpleDateFormat("yyyyMMdd")
        return fmt.format(Date(curr.since)).equals(fmt.format(Date(to)))
    }

    fun isPast(): Boolean {
        return System.currentTimeMillis() > to
    }

    fun isOngoing(): Boolean {
        /* val debugMillis = 1702893600000;
        val debugGap = 40 * 60 * 1000;
        
        if (since in debugMillis - debugGap .. debugMillis + debugGap) {
            return true
        } */
        
        val millis = System.currentTimeMillis()
        return millis in since..to
    }

    fun fancyDescription(): String {
        return teachers.joinToString(", ")
    }
}
