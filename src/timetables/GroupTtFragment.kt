package com.somobu.timetable.timetables

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.somobu.sutmodel.GroupsJson
import com.somobu.timetable.API
import com.somobu.timetable.R

class GroupTtFragment : AbsTtFragment() {

    override fun getSelectionRef(): TimetableVM.Ref {
        return TimetableVM.Ref(TimetableVM.Src.BY_GROUP, requireArguments().getString("group")!!)
    }
    
    override fun navToGroup(group: Int) {
        val bundle = Bundle()
        bundle.putString("group", group.toString())
        findNavController().navigate(R.id.action_groupTtFragment_self, bundle)
    }

    override fun navToTeacher(teacher: String) {
        val bundle = Bundle()
        bundle.putString("teacher", teacher)
        findNavController().navigate(R.id.action_groupTtFragment_to_teacherTtFragment, bundle)
    }

    override fun eventsLoaded(groups: GroupsJson, data: List<MyRecyclerAdapter.Entry>) {
        (activity as AppCompatActivity).supportActionBar?.subtitle = API.digGroupName(groups, getSelectionRef().id)
    }
}