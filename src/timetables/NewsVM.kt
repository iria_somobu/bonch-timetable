package com.somobu.timetable.timetables

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.GsonBuilder
import com.somobu.sutmodel.NewsEntry
import com.somobu.timetable.API
import com.somobu.timetable.BuildConfig

class NewsVM(val myapp: android.app.Application) : AndroidViewModel(myapp) {

    class NewsData {
        var isLoaded = false
        var e: Exception? = null
        var lastEntry: NewsEntry? = null
        var lastNonDismissibleEntry: NewsEntry? = null
    }

    private val data: MutableLiveData<NewsData> by lazy {
        MutableLiveData(NewsData()).also { loadNews() }
    }

    private fun loadNews() {
        object : Thread() {

            override fun run() {
                val d = NewsData()
                d.isLoaded = true

                println("Load news")

                try {
                    d.lastEntry = getFilteredEntry(API.getNews(myapp.applicationContext), false)
                    d.lastNonDismissibleEntry = getFilteredEntry(API.getNews(myapp.applicationContext), true)
                    println("Data loaded")
                } catch (e: java.lang.Exception) {
                    d.e = e
                    println("Data failed")
                    e.printStackTrace()
                }

                data.postValue(d)
            }

        }.start()
    }

    @Suppress("NAME_SHADOWING")
    private fun getFilteredEntry(list: Array<NewsEntry>, nonDismissibleOnly: Boolean): NewsEntry? {
        val date = System.currentTimeMillis();
        val ver = BuildConfig.VERSION_CODE;
        val isRelease = !BuildConfig.DEBUG;

        val list: List<NewsEntry> = list.filter { it != null }.sortedBy { -it.time }

        for (item in list) {
            if (nonDismissibleOnly && item.dismissible) continue;

            if (item.filter != null) {
                if (item.filter.sinceDate != 0L && item.filter.sinceDate > date) continue;
                if (item.filter.untilDate != 0L && item.filter.untilDate < date) continue;

                if (item.filter.sinceVer != 0 && item.filter.sinceVer >= ver) continue;
                if (item.filter.untilVer != 0 && item.filter.untilVer <= ver) continue;
                
                if (item.filter.debugOnly && isRelease) continue;
            }

            return item;
        }

        return null;
    }

    fun news(): LiveData<NewsData> {
        return data
    }

    fun testFilter() {

        val rawJson = """
            [
                { "time": 1702931949741, "title": "Dat title (1)" },
                { "time": 1702931949742, "title": "Dat title (2)", "filter": { "sinceVer": 32 } },
                { "time": 1702931949743, "title": "Dat title (3)", "filter": { "untilVer": 30 } }
            ]
        """.trimIndent()

        val gson = GsonBuilder().create();
        val items = gson.fromJson(rawJson, Array<NewsEntry>::class.java)!!

        println("Filtered entry:")
//        println(getFilteredEntry(items)!!.title)
    }

}