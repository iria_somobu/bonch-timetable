package com.somobu.timetable.timetables

import com.somobu.sutmodel.GroupsJson
import com.somobu.sutmodel.Pair

@Suppress("SameParameterValue")
object PairsToEvents {

    fun convert(
        pairs: List<Pair>,
        groups: GroupsJson,
        subjects: Map<Int, String>,
        teachers: Map<Int, String>
    ): List<MyRecyclerAdapter.Entry> {
        val result = arrayListOf<MyRecyclerAdapter.Entry>()

        for (pair in pairs) {
            val duration = getDuration(pair.pair)
            val location = abbrAddr(pair.aud)

            val title = subjects[pair.subject] ?: "Unknown pair (${pair.subject})"

            val teachersNames = pair.teachers.map { teachers[it] ?: "" }
            val teachersIds = pair.teachers.map { it }

            val groupNames = pair.groups.map { groups.groups[it.toString()] ?: it.toString() }
            val groupIds = pair.groups.map { it }

            for (since in pair.starts) {
                val to = since + duration

                result.add(
                    MyRecyclerAdapter.PairEntry(
                        Event(
                            pair.pair, getHumanPairNo(pair),
                            since, to,
                            title, location, pair.type,
                            teachersNames, teachersIds,
                            groupNames, groupIds
                        )
                    )
                )
            }
        }

        result.sortWith { p0, p1 -> cmp(p0!!, p1!!) }


        val eee = result.size
        
        if (eee >= 1) {
            result.add(MyRecyclerAdapter.DividerEntry(result[0].date - 1, false));
        }
        
        for (i in 0..eee - 2) {
            val currentPair = result[i] as MyRecyclerAdapter.PairEntry;
            val nextPair = result[i + 1] as MyRecyclerAdapter.PairEntry;

            if (!currentPair.pair.atTheSameDay(nextPair.pair)) {
                result.add(MyRecyclerAdapter.DividerEntry(nextPair.date - 1, false));
            }

            if (i == (eee - 2)) {
                result.add(MyRecyclerAdapter.DividerEntry(nextPair.date + 1, true));
            }
        }

        result.sortWith { p0, p1 -> cmp(p0!!, p1!!) }

        return result
    }

    private fun cmp(p0: MyRecyclerAdapter.Entry, p1: MyRecyclerAdapter.Entry): Int {
        val e = p0.date.compareTo(p1.date)
        if (e != 0) return e

        if (p0 is MyRecyclerAdapter.PairEntry) {
            if (p1 is MyRecyclerAdapter.PairEntry) {
                return p0.pair.title.compareTo(p1.pair.title)
            } else {
                return 1
            }
        }

        return 0
    }

    private fun getDuration(pair: Int): Long {
        return when (pair) {
            83, 84, 85, 86, 87 -> duration(1, 30)
            69 -> duration(0, 45)
            else -> duration(1, 35)
        }
    }

    private fun duration(hour: Int, minutes: Int): Long {
        return (hour * 60 * 60 * 1000 + minutes * 60 * 1000).toLong()
    }

    private fun abbrAddr(location: String?): String {
        if (location == null) return "Неизв."
        return location
            .replace("-Б22", "")
            .replace("; Б22", "")
            .replace("Спортивные площадки", "Спортплощадка")
    }

    /**
     * Человеческий номер пары (где 9:00 - это пара 1)
     */
    private fun getHumanPairNo(ev: Pair): String {
        // Почему-то у "нормальных" пар номера смещены "вверх"
        // т.е. 1-я пара (9 утра) имеет внутренний номер два
        if (ev.pair in 1..9) return (ev.pair - 1).toString()
        else return ""
    }

}