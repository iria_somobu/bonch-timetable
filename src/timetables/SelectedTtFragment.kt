package com.somobu.timetable.timetables

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.somobu.timetable.R
import com.somobu.sutmodel.GroupsJson
import com.somobu.timetable.API
import com.somobu.timetable.MainActivity
import com.somobu.timetable.startup.StartupActivity


open class SelectedTtFragment : AbsTtFragment() {

    override fun getSelectionRef(): TimetableVM.Ref {
        val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        return TimetableVM.Ref(TimetableVM.Src.BY_GROUP, prefs.getString(MainActivity.GROUP_ID, null)!!)
    }

    override fun navToGroup(group: Int) {
        val bundle = Bundle()
        bundle.putString("group", group.toString())
        findNavController().navigate(R.id.action_timetableListFragment_to_groupTtFragment, bundle)
    }

    override fun navToTeacher(teacher: String) {
        val bundle = Bundle()
        bundle.putString("teacher", teacher)
        findNavController().navigate(R.id.action_timetableListFragment_to_teacherTtFragment, bundle)
    }

    override fun eventsLoaded(groups: GroupsJson, data: List<MyRecyclerAdapter.Entry>) {
        (activity as AppCompatActivity).supportActionBar?.subtitle = API.digGroupName(groups, getSelectionRef().id)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.selected_tt, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === R.id.select_table) {
            val intent = Intent(requireContext(), StartupActivity::class.java)
            intent.putExtra(StartupActivity.FORCE_PICK, true)
            startActivity(intent)
            activity?.finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}