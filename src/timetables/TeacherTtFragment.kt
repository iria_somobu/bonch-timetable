package com.somobu.timetable.timetables

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.somobu.timetable.R
import com.somobu.sutmodel.GroupsJson

class TeacherTtFragment : AbsTtFragment() {

    override fun getSelectionRef(): TimetableVM.Ref {
        return TimetableVM.Ref(TimetableVM.Src.BY_PREPOD, requireArguments().getString("teacher")!!)
    }

    override fun navToGroup(group: Int) {
        val bundle = Bundle()
        bundle.putString("group", group.toString())
        findNavController().navigate(R.id.action_teacherTtFragment_to_groupTtFragment, bundle)
    }

    override fun navToTeacher(teacher: String) {
        val bundle = Bundle()
        bundle.putString("teacher", teacher)
        findNavController().navigate(R.id.action_teacherTtFragment_self, bundle)
    }

    override fun eventsLoaded(groups: GroupsJson, data: List<MyRecyclerAdapter.Entry>) {
        (activity as AppCompatActivity).supportActionBar?.subtitle = getSelectionRef().id
    }
}