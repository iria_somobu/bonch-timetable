package com.somobu.timetable.timetables

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.somobu.timetable.R
import com.somobu.timetable.AbsOverlayFragment

class TeachersListFragment : AbsOverlayFragment() {

    private val teachersVM: TeachersVM by activityViewModels()

    override fun onCreateContent(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.content_prepod_chooser, container, false)
    }

    override fun onResume() {
        super.onResume()

        teachersVM.teachers().observe(this) { teachers: TeachersVM.TeachersData ->
            if (teachers.isLoaded) {
                if (teachers.data != null) onEventsLoaded(teachers.data!!)
                if (teachers.e != null) setError(teachers.e)
            } else {
                setPleaseWait()
            }
        }
    }

    override fun onRetry() {
        teachersVM.loadTeachers()
    }

    private fun onEventsLoaded(data: Array<String>) {
        setContent()

        val root = view ?: return
        val tv = root.findViewById<AutoCompleteTextView>(R.id.autoCompleteTextView)
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, data)

        tv.setAdapter(adapter)
        tv.setOnItemClickListener { _, _, i, _ ->
            val bundle = Bundle()
            bundle.putString("teacher", adapter.getItem(i))
            findNavController().navigate(R.id.action_teachersListFragment_to_teacherTtFragment, bundle)
            tv.setText("")
        }
    }

}
