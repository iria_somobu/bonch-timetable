package com.somobu.timetable.timetables

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.somobu.timetable.API
import com.somobu.timetable.groups.GroupsVM

class TeachersVM(val myapp: android.app.Application) : AndroidViewModel(myapp) {

    class TeachersData {
        var isLoaded = false
        var e: Exception? = null
        var data: Array<String>? = null
    }

    private val data: MutableLiveData<TeachersData> by lazy {
        MutableLiveData(TeachersData()).also { loadTeachers() }
    }

    fun loadTeachers() {
        object : Thread() {

            override fun run() {
                val d = TeachersData()
                d.isLoaded = true

                try {
                    d.data = API.getTeachers(myapp.applicationContext)
                } catch (e: java.lang.Exception) {
                    d.e = e
                }

                data.postValue(d)
            }

        }.start()
    }

    fun teachers(): LiveData<TeachersData> {
        return data
    }

}