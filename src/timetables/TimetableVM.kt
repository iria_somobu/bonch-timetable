package com.somobu.timetable.timetables

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.somobu.sutmodel.GroupsJson
import com.somobu.sutmodel.Pair
import com.somobu.timetable.API

class TimetableVM(val myapp: android.app.Application) : AndroidViewModel(myapp) {

    enum class Src {
        BY_GROUP,
        BY_PREPOD
    }

    class Ref {
        val src: Src
        var id: String

        constructor(ref: String) {
            val split = ref.split(":")

            if (split[0] == "group") {
                src = Src.BY_GROUP
            } else {
                src = Src.BY_PREPOD
            }

            id = split[1]
        }

        constructor(_src: Src, _id: String) {
            src = _src
            id = _id
        }

        override fun equals(other: Any?): Boolean {
            if (other !is Ref) return false
            return src == other.src && id == other.id
        }

        override fun hashCode(): Int {
            var result = src.hashCode()
            result = 31 * result + id.hashCode()
            return result
        }

        fun toStringRef() {
            val type = when (src) {
                Src.BY_GROUP -> "group"
                Src.BY_PREPOD -> "prepod"
            }
        }
    }

    class TimetableData {
        var ref: Ref? = null

        var isLoaded = false

        var e: Exception? = null

        var data: List<MyRecyclerAdapter.Entry>? = null
        var firstPresentRef: MyRecyclerAdapter.PairEntry? = null
        var groups: GroupsJson? = null
    }

    private val data: MutableLiveData<TimetableData> by lazy {
        MutableLiveData(TimetableData())
    }

    fun timetable(ref: Ref): LiveData<TimetableData> {
        val tdata = data.value!!

        if (tdata.ref != ref) loadTable(ref)

        return data
    }

    fun loadTable(ref: Ref) {
        object : Thread() {

            override fun run() {
                val d = TimetableData()
                d.isLoaded = true

                try {

                    val pairs: List<Pair>
                    val groups = API.getGroups(myapp)
                    val subjects: Map<Int, String>
                    var teachers: Map<Int, String> = HashMap()

                    if (ref.src == Src.BY_GROUP) {
                        val tt = API.getGroupTimetable(myapp, ref.id)
                        pairs = tt.pairs
                        subjects = tt.subjects
                        teachers = tt.teachers
                    } else {
                        val tt = API.getTeacherTimetable(myapp, ref.id)
                        pairs = tt.pairs
                        subjects = tt.subjects
                    }

                    d.data = PairsToEvents.convert(pairs, groups, subjects, teachers)
                    d.groups = groups

                    d.firstPresentRef = null;
                    for (i in 0 until (d.data!!.size -1)) {
                        val item = d.data!![i]
                        if (item is MyRecyclerAdapter.PairEntry && !item.pair.isPast()) {
                            d.firstPresentRef = item
                            break;
                        }
                    }

                } catch (e: java.lang.Exception) {
                    d.e = e
                }

                data.postValue(d)
            }

        }.start()
    }
}